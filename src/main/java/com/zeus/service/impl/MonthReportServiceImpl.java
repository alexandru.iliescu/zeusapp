package com.zeus.service.impl;

import com.zeus.exceptions.ReportException;
import com.zeus.model.dto.MonthReportDTO;
import com.zeus.model.entity.DayReport;
import com.zeus.model.entity.MonthReport;
import com.zeus.repository.DayReportRepository;
import com.zeus.repository.MonthReportRepository;
import com.zeus.service.MonthReportService;
import com.zeus.service.util.MonthReportServiceUtil;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class MonthReportServiceImpl implements MonthReportService {

    private DayReportRepository dayReportRepository;
    private MonthReportRepository monthReportRepository;
    private MonthReportServiceUtil monthReportUtil;
    private ModelMapper modelMapper;

    @Autowired
    public MonthReportServiceImpl(DayReportRepository dayReportRepository,
            MonthReportRepository monthReportRepository,
            MonthReportServiceUtil monthReportUtil, ModelMapper modelMapper) {
        this.dayReportRepository = dayReportRepository;
        this.monthReportRepository = monthReportRepository;
        this.monthReportUtil = monthReportUtil;
        this.modelMapper = modelMapper;
    }

    @Override
    public MonthReportDTO saveUserMonthReport(MonthReportDTO monthReportDTO) {
        try {
            List<DayReport> dayReports = dayReportRepository.findDayReportsByUserId(monthReportDTO.getUserId());
            List<DayReport> validDayReports = new ArrayList<>();

            LocalDate userInputDate = monthReportUtil.definedMonthInterval(monthReportDTO);
            dayReports.forEach(dayReport -> {
                if (userInputDate.getMonth().equals(dayReport.getDateForReport().getMonth())) {
                    validDayReports.add(dayReport);
                }
            });

            MonthReport monthReport = modelMapper.map(monthReportDTO, MonthReport.class);
            monthReport.setMonthForReport(Month.valueOf(monthReportDTO.getMonthForReport().toUpperCase()));
            monthReport.setWorkedHours(monthReportUtil.getWorkedHours(validDayReports));

            return modelMapper.map(monthReportRepository.save(monthReport), MonthReportDTO.class);
        } catch (DataAccessException e) {
            throw new ReportException("Error occured", HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}