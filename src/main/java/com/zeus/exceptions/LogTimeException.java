package com.zeus.exceptions;

public class LogTimeException extends CustomApplicationException {

    public LogTimeException(String message, int statusCode) {
        super(message, statusCode);
    }

    public LogTimeException(String message, Throwable throwable, int statusCode) {
        super(message, throwable, statusCode);
    }

    public LogTimeException() {
    }
}