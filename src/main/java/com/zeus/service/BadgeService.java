package com.zeus.service;

import com.zeus.model.dto.BadgeDTO;
import org.springframework.transaction.annotation.Transactional;

public interface BadgeService {

    @Transactional
    BadgeDTO saveBadge(BadgeDTO badgeDTO);
}