package com.zeus.model.entity;

import java.io.Serializable;
import java.sql.Time;
import java.time.Month;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "monthReports")
@Entity
@Data
@NoArgsConstructor
public class MonthReport implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private Month monthForReport;
    private Time workedHours;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "userId_fk")
    private User user;
}