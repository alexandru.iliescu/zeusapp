package com.zeus.service.impl;

import com.zeus.exceptions.LogTimeException;
import com.zeus.model.dto.UserLogDTO;
import com.zeus.model.entity.UserLog;
import com.zeus.repository.UserLogRepository;
import com.zeus.service.UserLogService;
import com.zeus.service.util.LogTimeServiceUtil;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserLogServiceImpl implements UserLogService {

    private UserLogRepository userLogRepository;
    private ModelMapper modelMapper;
    private LogTimeServiceUtil logTimeServiceUtil;

    @Autowired
    private UserLogServiceImpl(UserLogRepository userLogRepository,
            ModelMapper modelMapper, LogTimeServiceUtil logTimeServiceUtil) {
        this.userLogRepository = userLogRepository;
        this.modelMapper = modelMapper;
        this.logTimeServiceUtil = logTimeServiceUtil;
    }

    public UserLogServiceImpl() {
    }

    @Override
    @Transactional
    public UserLogDTO saveUserLog(UserLogDTO userLogDTO) {
        try {
            logTimeServiceUtil.logTypeValidation(userLogDTO);
            logTimeServiceUtil.setUserLogTimeAfterLogType(userLogDTO);

            UserLog userLog = modelMapper.map(userLogDTO, UserLog.class);

            return modelMapper.map(logTimeServiceUtil.saveLogTimeHelper(userLog), UserLogDTO.class);
        } catch (DataAccessException e) {
            throw new LogTimeException("Error occured", HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    @Transactional
    public UserLogDTO updateUserLog(UserLogDTO userLogDTO) {
        try {
            logTimeServiceUtil.logTypeValidation(userLogDTO);
            UserLog userLog = modelMapper.map(userLogDTO, UserLog.class);
            return modelMapper.map(logTimeServiceUtil.saveLogTimeHelper(userLog), UserLogDTO.class);
        } catch (
                DataAccessException e) {
            throw new LogTimeException("Error occured", HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    public Optional<UserLogDTO> getUserLogById(Long id) {
        return userLogRepository.findById(id)
                .map(userLog -> logTimeServiceUtil.logTimeToDTO(userLog));
    }

    @Override
    @Transactional
    public void deleteUserLogById(Long id) {
        userLogRepository.deleteById(id);
    }

    @Override
    public List<UserLogDTO> findLogTimeByBadge(Long badge) {
        List<UserLog> userLogDTODTOList = userLogRepository.findLogTimeByBadge(badge);
        return userLogDTODTOList.stream()
                .map(userLog -> logTimeServiceUtil.logTimeToDTO(userLog))
                .collect(Collectors.toList());
    }
}