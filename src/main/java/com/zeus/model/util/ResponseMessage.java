package com.zeus.model.util;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class ResponseMessage {

    private Object responseObject;
    private ResponseStatus responseStatus;
    private List<String> errors = new ArrayList<>();

    public ResponseMessage() {
    }

    public ResponseMessage(Object responseObject, ResponseStatus responseStatus) {
        this.responseObject = responseObject;
        this.responseStatus = responseStatus;
    }

    public ResponseMessage(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getResponseObject() {
        return responseObject;
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}