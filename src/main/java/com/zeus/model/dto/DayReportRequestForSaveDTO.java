package com.zeus.model.dto;

import com.zeus.model.entity.UserLog;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDate;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DayReportRequestForSaveDTO implements Serializable {

    private Long id;
    private LocalDate dateForReport;
    private Time workedHours;
    private Long userId;
    private List<UserLog> userLogs;
}