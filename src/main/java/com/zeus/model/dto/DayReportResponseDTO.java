package com.zeus.model.dto;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DayReportResponseDTO implements Serializable {

    private Long badgeNumber;
    private String firstName;
    private String lastName;
    private Time workedHours;
    private List<UserLogDTO> userLogs;
}