package com.zeus.service.util;

import com.zeus.exceptions.ReportException;
import com.zeus.model.dto.MonthReportDTO;
import com.zeus.model.entity.DayReport;
import java.sql.Time;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

@Component
public class MonthReportServiceUtil {

    private static final int TIME_OFFSET = 7200000;

    public Time getWorkedHours(List<DayReport> validDayReports) {
        List<Long> times = new ArrayList<>();
        validDayReports.forEach(report ->
                times.add((long) (report.getWorkedHours().toLocalTime().getSecond() * 1000))
        );
        long timeInMillis = times.stream().mapToInt(Long::intValue).sum();
        return new Time(timeInMillis - TIME_OFFSET);
    }

    public LocalDate definedMonthInterval(MonthReportDTO monthReportDTO) {
        Month month = Month.valueOf(monthReportDTO.getMonthForReport().toUpperCase());
        switch (month) {
            case JANUARY:
                return LocalDate.of(2020, 1, 1);
            case FEBRUARY:
                return LocalDate.of(2020, 2, 1);
            case MARCH:
                return LocalDate.of(2020, 3, 1);
            case APRIL:
                return LocalDate.of(2020, 4, 1);
            case MAY:
                return LocalDate.of(2020, 5, 1);
            case JUNE:
                return LocalDate.of(2020, 6, 1);
            case JULY:
                return LocalDate.of(2020, 7, 1);
            case AUGUST:
                return LocalDate.of(2020, 8, 1);
            case SEPTEMBER:
                return LocalDate.of(2020, 9, 1);
            case OCTOBER:
                return LocalDate.of(2020, 10, 1);
            case NOVEMBER:
                return LocalDate.of(2020, 11, 1);
            case DECEMBER:
                return LocalDate.of(2020, 12, 1);
            default:
                throw new ReportException("Wrong value for month!",
                        HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}