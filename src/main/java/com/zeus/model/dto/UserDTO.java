package com.zeus.model.dto;

import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDTO implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String position;
    private Long badgeId;

    public UserDTO(Long id, String firstName, String lastName, Long badgeId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.badgeId = badgeId;
    }
}