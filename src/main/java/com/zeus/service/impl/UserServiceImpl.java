package com.zeus.service.impl;

import com.zeus.exceptions.LogTimeException;
import com.zeus.model.dto.UserDTO;
import com.zeus.model.entity.User;
import com.zeus.repository.UserRepository;
import com.zeus.service.UserService;
import com.zeus.service.util.UserServiceUtil;
import javax.servlet.http.HttpServletResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    private ModelMapper modelMapper;
    private UserRepository userRepository;
    private UserServiceUtil userServiceUtil;

    @Autowired
    public UserServiceImpl(ModelMapper modelMapper, UserRepository userRepository,
            UserServiceUtil userServiceUtil) {
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
        this.userServiceUtil = userServiceUtil;
    }

    @Override
    @Transactional
    public UserDTO saveUser(UserDTO userDTO) {
        try {
            User user = modelMapper.map(userDTO, User.class);
            userServiceUtil.validatePosition(userDTO, user);

            return modelMapper.map(userRepository.save(user), UserDTO.class);
        } catch (DataAccessException e) {
            throw new LogTimeException("Error occured", HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    @Transactional
    public UserDTO getUserByBadge(Long badgeNumber) {
        User user = userRepository.findUserByBadge(badgeNumber);

        return modelMapper.map(user, UserDTO.class);
    }
}