package com.zeus.service;

import com.zeus.model.dto.MonthReportDTO;
import org.springframework.transaction.annotation.Transactional;

public interface MonthReportService {

    @Transactional
    MonthReportDTO saveUserMonthReport(MonthReportDTO monthReportDTO);
}