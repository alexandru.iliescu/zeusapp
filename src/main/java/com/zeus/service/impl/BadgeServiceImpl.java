package com.zeus.service.impl;

import com.zeus.exceptions.LogTimeException;
import com.zeus.model.dto.BadgeDTO;
import com.zeus.model.entity.Badge;
import com.zeus.repository.BadgeRepository;
import com.zeus.service.BadgeService;
import java.time.Instant;
import java.util.Date;
import javax.servlet.http.HttpServletResponse;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@NoArgsConstructor
@Service
public class BadgeServiceImpl implements BadgeService {

    private ModelMapper modelMapper;
    private BadgeRepository badgeRepository;

    @Autowired
    private BadgeServiceImpl(ModelMapper modelMapper, BadgeRepository badgeRepository) {
        this.modelMapper = modelMapper;
        this.badgeRepository = badgeRepository;
    }

    @Override
    public BadgeDTO saveBadge(BadgeDTO badgeDTO) {
        try {
            badgeDTO.setBadgeIssuedDate(Date.from(Instant.now()));
            Badge badge = modelMapper.map(badgeDTO, Badge.class);

            return modelMapper.map(badgeRepository.save(badge), BadgeDTO.class);
        } catch (DataAccessException e) {
            throw new LogTimeException("Error occured", HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}