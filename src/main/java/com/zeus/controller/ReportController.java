package com.zeus.controller;

import static com.zeus.model.util.ResponseStatus.GET_USER_DAY_REPORT_BY_BADGE_SUCCESSFUL;
import static com.zeus.model.util.ResponseStatus.USER_DAY_REPORT_SAVED_SUCCESSFUL;
import static com.zeus.model.util.ResponseStatus.USER_MONTH_REPORT_SAVED_SUCCESSFUL;

import com.zeus.model.dto.DayReportRequestForGetDTO;
import com.zeus.model.dto.DayReportRequestForSaveDTO;
import com.zeus.model.dto.MonthReportDTO;
import com.zeus.model.dto.UserDTO;
import com.zeus.model.util.ResponseMessage;
import com.zeus.service.DayReportService;
import com.zeus.service.MonthReportService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReportController {

    private DayReportService dayReportService;
    private MonthReportService monthReportService;

    @Autowired
    private ReportController(DayReportService dayReportService, MonthReportService monthReportService) {
        this.dayReportService = dayReportService;
        this.monthReportService = monthReportService;
    }

    @ApiOperation(value = "Get user day report by badge number")
    @PostMapping("/api/dayReport")
    public ResponseMessage getUserDayReportByBadge(@RequestBody DayReportRequestForGetDTO dayReportRequestForGetDTO) {
        return new ResponseMessage(dayReportService.getUserDayReport(dayReportRequestForGetDTO),
                GET_USER_DAY_REPORT_BY_BADGE_SUCCESSFUL);
    }

    @ApiOperation(value = "Save user day report", response = UserDTO.class)
    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value = "/api/dayReport/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseMessage saveUserDayReport(@RequestBody DayReportRequestForSaveDTO dayReportRequestForSaveDTO) {
        return new ResponseMessage(dayReportService.saveUserDayReport(dayReportRequestForSaveDTO),
                USER_DAY_REPORT_SAVED_SUCCESSFUL);
    }

    @ApiOperation(value = "Save user month report", response = MonthReportDTO.class)
    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value = "/api/monthReport/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseMessage saveUserMonthReport(@RequestBody MonthReportDTO monthReportDTO) {
        return new ResponseMessage(monthReportService.saveUserMonthReport(monthReportDTO),
                USER_MONTH_REPORT_SAVED_SUCCESSFUL);
    }
}