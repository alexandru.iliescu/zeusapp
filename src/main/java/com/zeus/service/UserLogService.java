package com.zeus.service;

import com.zeus.model.dto.UserLogDTO;
import java.util.List;
import java.util.Optional;
import org.springframework.transaction.annotation.Transactional;

public interface UserLogService {

    @Transactional
    UserLogDTO saveUserLog(UserLogDTO userLogDTO);

    @Transactional
    UserLogDTO updateUserLog(UserLogDTO userLogDTO);

    @Transactional
    Optional<UserLogDTO> getUserLogById(Long id);

    @Transactional
    void deleteUserLogById(Long id);

    @Transactional
    List<UserLogDTO> findLogTimeByBadge(Long badge);
}