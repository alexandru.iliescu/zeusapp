package com.zeus.controller;

import static com.zeus.model.util.ResponseStatus.BADGE_SAVED_SUCCESSFUL;

import com.zeus.model.dto.BadgeDTO;
import com.zeus.model.util.ResponseMessage;
import com.zeus.service.BadgeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BadgeController {

    private BadgeService badgeService;

    @Autowired
    private BadgeController(BadgeService badgeService) {
        this.badgeService = badgeService;
    }

    @ApiOperation(value = "Save badge", response = BadgeDTO.class)
    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value = "/api/badge", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseMessage saveLogTime(@RequestBody BadgeDTO badgeDTO) {
        return new ResponseMessage(badgeService.saveBadge(badgeDTO), BADGE_SAVED_SUCCESSFUL);
    }
}