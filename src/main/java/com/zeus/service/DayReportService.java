package com.zeus.service;

import com.zeus.model.dto.DayReportRequestForGetDTO;
import com.zeus.model.dto.DayReportRequestForSaveDTO;
import com.zeus.model.dto.DayReportResponseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface DayReportService {

    @Transactional
    DayReportResponseDTO getUserDayReport(DayReportRequestForGetDTO dayReportRequestForGetDTO);

    @Transactional
    DayReportRequestForSaveDTO saveUserDayReport(DayReportRequestForSaveDTO dayReportRequestForSaveDTO);
}