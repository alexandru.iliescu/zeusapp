package com.zeus.model.dto;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserLogDTO implements Serializable {

    private Long id;

    @NotEmpty(message = "logTime.logType.required")
    private String logType;
    private Date userLogTime;
    private Long userId;
}