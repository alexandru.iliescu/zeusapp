package com.zeus.repository;

import com.zeus.model.entity.DayReport;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DayReportRepository extends JpaRepository<DayReport, Long> {

    @Query(value = "SELECT * from dayreports where userId_fk =:id", nativeQuery = true)
    List<DayReport> findDayReportsByUserId(@Param("id") Long id);
}