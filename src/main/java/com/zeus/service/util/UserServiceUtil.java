package com.zeus.service.util;

import com.zeus.exceptions.UserException;
import com.zeus.model.dto.UserDTO;
import com.zeus.model.entity.User;
import com.zeus.model.util.Position;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

@Component
public class UserServiceUtil {

    public void validatePosition(UserDTO userDTO, User user) {
        Position position = Position.getValueOf(userDTO.getPosition());
        if (position == null) {
            throw new UserException("Invalid position value!", HttpServletResponse.SC_BAD_REQUEST);
        }
        switch (position) {
            case ADMINISTRATION:
                user.setPosition(Position.ADMINISTRATION);
                break;
            case DEVELOPER:
                user.setPosition(Position.DEVELOPER);
                break;
            case QA:
                user.setPosition(Position.QA);
                break;
            case UI_UX:
                user.setPosition(Position.UI_UX);
                break;
            case SCRUM_MASTER:
                user.setPosition(Position.SCRUM_MASTER);
                break;
            case PO:
                user.setPosition(Position.PO);
                break;
        }
    }
}