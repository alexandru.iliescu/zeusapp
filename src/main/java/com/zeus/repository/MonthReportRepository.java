package com.zeus.repository;

import com.zeus.model.entity.MonthReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonthReportRepository extends JpaRepository<MonthReport, Long> {

}