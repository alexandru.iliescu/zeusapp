package com.zeus.model.dto;

import java.io.Serializable;
import java.sql.Time;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MonthReportDTO implements Serializable {

    private Long id;
    private String monthForReport;
    private Time workedHours;
    private Long userId;
}
