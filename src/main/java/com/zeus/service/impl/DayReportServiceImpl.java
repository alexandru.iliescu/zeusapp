package com.zeus.service.impl;

import com.zeus.exceptions.ReportException;
import com.zeus.model.dto.DayReportRequestForGetDTO;
import com.zeus.model.dto.DayReportRequestForSaveDTO;
import com.zeus.model.dto.DayReportResponseDTO;
import com.zeus.model.dto.UserDTO;
import com.zeus.model.dto.UserLogDTO;
import com.zeus.model.entity.DayReport;
import com.zeus.model.entity.User;
import com.zeus.model.entity.UserLog;
import com.zeus.repository.DayReportRepository;
import com.zeus.repository.UserLogRepository;
import com.zeus.repository.UserRepository;
import com.zeus.service.DayReportService;
import com.zeus.service.util.DayReportServiceUtil;
import java.sql.Time;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@NoArgsConstructor
@Service
public class DayReportServiceImpl implements DayReportService {

    private UserLogRepository userLogRepository;
    private UserRepository userRepository;
    private ModelMapper modelMapper;
    private DayReportServiceUtil reportUtil;
    private DayReportRepository reportRepository;

    @Autowired
    public DayReportServiceImpl(UserLogRepository userLogRepository,
            UserRepository userRepository, ModelMapper modelMapper,
            DayReportServiceUtil reportUtil, DayReportRepository reportRepository) {
        this.userLogRepository = userLogRepository;
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.reportUtil = reportUtil;
        this.reportRepository = reportRepository;
    }

    @Override
    public DayReportResponseDTO getUserDayReport(DayReportRequestForGetDTO dayReportRequestForGetDTO) {
        List<UserLog> userLogs = userLogRepository.findUserDayReportByBadge(dayReportRequestForGetDTO.getBadgeNumber());

        long userLogWithoutOffset = reportUtil.getUserLogsInMillis(dayReportRequestForGetDTO, userLogs);
        Time workedHours = new Time(userLogWithoutOffset);

        List<UserLogDTO> userLogDTOs = modelMapper.map(userLogs, new TypeToken<List<UserLogDTO>>() {
        }.getType());
        List<UserLogDTO> userLogsForDayReport = reportUtil
                .getValidUserLogsForDayReport(dayReportRequestForGetDTO, userLogDTOs);
        User user = userRepository.findUserByBadge(dayReportRequestForGetDTO.getBadgeNumber());
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);

        DayReportResponseDTO dayReportResponseDTO = new DayReportResponseDTO();
        dayReportResponseDTO.setBadgeNumber(dayReportRequestForGetDTO.getBadgeNumber());
        dayReportResponseDTO.setFirstName(userDTO.getFirstName());
        dayReportResponseDTO.setLastName(userDTO.getLastName());
        dayReportResponseDTO.setWorkedHours(workedHours);
        dayReportResponseDTO.setUserLogs(userLogsForDayReport);

        return dayReportResponseDTO;
    }

    @Override
    @Transactional
    public DayReportRequestForSaveDTO saveUserDayReport(DayReportRequestForSaveDTO dayReportRequestForSaveDTO) {
        try {
            DayReport dayReport = modelMapper.map(dayReportRequestForSaveDTO, DayReport.class);

            return modelMapper.map(reportRepository.save(dayReport), DayReportRequestForSaveDTO.class);
        } catch (DataAccessException e) {
            throw new ReportException("Error occured", HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}