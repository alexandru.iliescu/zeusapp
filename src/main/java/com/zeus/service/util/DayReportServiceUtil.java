package com.zeus.service.util;

import com.zeus.model.dto.DayReportRequestForGetDTO;
import com.zeus.model.dto.UserLogDTO;
import com.zeus.model.entity.UserLog;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class DayReportServiceUtil {

    private static final int TIME_OFFSET = 7200000;

    public List<UserLogDTO> getValidUserLogsForDayReport(DayReportRequestForGetDTO dayReportRequestForGetDTO,
            List<UserLogDTO> userLogDTOs) {
        List<UserLogDTO> validUserLogsForDayReport = new ArrayList<>();

        Date startOfDay = getStartOrEndDayList(dayReportRequestForGetDTO).get(0);
        Date endOfDay = getStartOrEndDayList(dayReportRequestForGetDTO).get(1);

        userLogDTOs.forEach(userLogDTO -> {
            boolean userLogIsValid =
                    userLogDTO.getUserLogTime().before(endOfDay) && userLogDTO.getUserLogTime().after(startOfDay);
            if (userLogIsValid) {
                validUserLogsForDayReport.add(userLogDTO);
            }
        });
        return validUserLogsForDayReport;
    }

    private List<Date> getStartOrEndDayList(DayReportRequestForGetDTO dayReportRequestForGetDTO) {
        LocalDate dateForReport = dayReportRequestForGetDTO.getDateForReport();
        LocalDateTime localStartOfDay = dateForReport.atStartOfDay().minusHours(2);
        LocalDateTime localEndOfDay = dateForReport.atStartOfDay().plusHours(21);

        Date startOfDay = Date.from(localStartOfDay.atZone(ZoneId.of("UTC")).toInstant());
        Date endOfDay = Date.from(localEndOfDay.atZone(ZoneId.of("UTC")).toInstant());

        List<Date> startOrEndDayList = new ArrayList<>();
        startOrEndDayList.add(startOfDay);
        startOrEndDayList.add(endOfDay);

        return startOrEndDayList;
    }

    public long getUserLogsInMillis(DayReportRequestForGetDTO dayReportRequestForGetDTO, List<UserLog> userLogs) {
        List<Date> datesIn = new ArrayList<>();
        List<Date> datesOut = new ArrayList<>();
        validateUserLogs(userLogs, datesIn, datesOut, dayReportRequestForGetDTO);

        datesIn.sort(Collections.reverseOrder());
        datesOut.sort(Collections.reverseOrder());

        userLogsList(datesIn, datesOut);
        long userLogsInMillis = userLogsList(datesIn, datesOut).stream().mapToInt(Long::intValue).sum();

        return userLogsInMillis - TIME_OFFSET;
    }

    private void validateUserLogs(List<UserLog> userLogList, List<Date> datesIn, List<Date> datesOut,
            DayReportRequestForGetDTO dayReportRequestForGetDTO) {
        Date startOfDay = getStartOrEndDayList(dayReportRequestForGetDTO).get(0);
        Date endOfDay = getStartOrEndDayList(dayReportRequestForGetDTO).get(1);

        userLogList.forEach(userLog -> {
            boolean validUserLog = userLog.getUserLogTime().before(endOfDay) && userLog.getUserLogTime().after(startOfDay);

            if (validUserLog) {
                if (userLog.getLogType().equalsIgnoreCase("in")) {
                    datesIn.add(userLog.getUserLogTime());
                } else if (userLog.getLogType().equalsIgnoreCase("out")) {
                    datesOut.add(userLog.getUserLogTime());
                }
            }
        });
    }

    private List<Long> userLogsList(List<Date> datesIn, List<Date> datesOut) {
        List<Long> longList = new ArrayList<>();
        for (int iteration = 0; iteration < datesOut.size(); iteration++) {
            longList.add(datesOut.get(iteration).getTime() - datesIn.get(iteration).getTime());
        }
        return longList;
    }
}