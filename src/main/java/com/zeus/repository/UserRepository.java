package com.zeus.repository;

import com.zeus.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT new User (e.id,  e.email, e.firstName, e.lastName, e.position, e.badge) FROM User e WHERE e.badge.badgeNumber =:badgeNumber")
    User findUserByBadge(@Param("badgeNumber") Long badgeNumber);
}