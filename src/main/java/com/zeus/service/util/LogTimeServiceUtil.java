package com.zeus.service.util;

import static java.time.Instant.now;

import com.zeus.exceptions.LogTimeException;
import com.zeus.model.dto.UserLogDTO;
import com.zeus.model.entity.UserLog;
import com.zeus.repository.UserLogRepository;
import java.util.Date;
import javax.servlet.http.HttpServletResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LogTimeServiceUtil {

    private ModelMapper modelMapper;
    private UserLogRepository userLogRepository;


    @Autowired
    public LogTimeServiceUtil(ModelMapper modelMapper, UserLogRepository userLogRepository) {
        this.modelMapper = modelMapper;
        this.userLogRepository = userLogRepository;
    }

    public void logTypeValidation(UserLogDTO userLogDTO) {
        if (!userLogDTO.getLogType().matches("(?:in|out)")) {
            throw new LogTimeException("logType can be only 'in' or 'out'!", HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    public void setUserLogTimeAfterLogType(UserLogDTO userLogDTO) {
        if (userLogDTO.getLogType() == null) {
            throw new LogTimeException("logType is required!", HttpServletResponse.SC_BAD_REQUEST);
        }
        if (userLogDTO.getLogType().matches("(?:in|out)")) {
            userLogDTO.setUserLogTime(Date.from(now()));
        }
    }

    public UserLogDTO logTimeToDTO(UserLog userLog) {
        return modelMapper.map(userLog, UserLogDTO.class);
    }

    public UserLog saveLogTimeHelper(UserLog userLog) {
        return userLogRepository.save(userLog);
    }
}