package com.zeus.model.dto;

import java.io.Serializable;
import java.time.LocalDate;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DayReportRequestForGetDTO implements Serializable {

    private Long badgeNumber;
    private LocalDate dateForReport;
}