package com.zeus;

import java.util.Locale;
import java.util.ResourceBundle;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@PropertySource("classpath:/application.properties")
@EnableCaching
@EnableSwagger2
@SpringBootApplication
public class ZeusApplication {

    private static final String RESOURCE_BUNDLE_NAME = "resourceMessages";

    public static void main(String[] args) {
        SpringApplication.run(ZeusApplication.class, args);
    }

    @Bean
    public ResourceBundle resourceBundleZeusMessages() {
        return ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME, Locale.getDefault());
    }
}