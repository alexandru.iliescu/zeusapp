package com.zeus.controller;

import static com.zeus.model.util.ResponseStatus.GET_USER_BY_BADGE;
import static com.zeus.model.util.ResponseStatus.USER_SAVED_SUCCESSFUL;

import com.zeus.model.dto.UserDTO;
import com.zeus.model.util.ResponseMessage;
import com.zeus.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(value = "Save user", response = UserDTO.class)
    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseMessage saveUser(@RequestBody UserDTO userDTO) {
        return new ResponseMessage(userService.saveUser(userDTO), USER_SAVED_SUCCESSFUL);
    }

    @ApiOperation(value = "Get user by badge number")
    @GetMapping("/api/user/{badgeNumber}")
    public ResponseMessage getUserById(@PathVariable("badgeNumber") Long badgeNumber) {
        return new ResponseMessage(userService.getUserByBadge(badgeNumber), GET_USER_BY_BADGE);
    }
}