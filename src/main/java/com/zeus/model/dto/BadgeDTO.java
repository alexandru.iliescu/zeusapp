package com.zeus.model.dto;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BadgeDTO implements Serializable {

    private Long id;
    private Long badgeNumber;
    private Date badgeIssuedDate;
    private String badgeAccessLevel;
}