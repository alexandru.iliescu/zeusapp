package com.zeus.model.util;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ResponseStatus {
    LOG_SAVED_SUCCESSFUL("User log saved successful!"),
    LOG_UPDATED_SUCCESSFUL("User log updated successful!"),
    GET_USER_LOG_BY_ID_SUCCESS("User log by id successful retrieved!"),
    FIND_USER_LOG_BY_BADGE_NUMBER_SUCCESS("User log by badge number successful retrieved!"),
    USER_LOG_DELETED_SUCCESS("UserLog deleted by id successful!"),
    USER_SAVED_SUCCESSFUL("User saved successful!"),
    BADGE_SAVED_SUCCESSFUL("Badge saved successful!"),
    GET_USER_DAY_REPORT_BY_BADGE_SUCCESSFUL("User day report by badge number successful retrieved!"),
    USER_DAY_REPORT_SAVED_SUCCESSFUL("User day report saved successful!"),
    USER_MONTH_REPORT_SAVED_SUCCESSFUL("User month report saved successful!"),
    GET_USER_BY_BADGE("Get user by badge number successful retrieved!");

    private String messageText;

    public String getMessageText() {
        return messageText;
    }

    ResponseStatus(String messageText) {
        this.messageText = messageText;
    }
}