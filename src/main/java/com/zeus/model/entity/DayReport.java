package com.zeus.model.entity;

import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "dayReports")
@Entity
@Data
@NoArgsConstructor
public class DayReport implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private LocalDate dateForReport;
    private Time workedHours;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "userId_fk")
    private User user;

    @OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "dayReportId_fk", referencedColumnName = "id")
    private List<UserLog> userLogs;

    public DayReport(Long id, LocalDate dateForReport, Time workedHours, User user) {
        this.id = id;
        this.dateForReport = dateForReport;
        this.workedHours = workedHours;
        this.user = user;
    }
}