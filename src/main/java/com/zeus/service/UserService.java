package com.zeus.service;

import com.zeus.model.dto.UserDTO;
import org.springframework.transaction.annotation.Transactional;

public interface UserService {

    @Transactional
    UserDTO saveUser(UserDTO userDTO);

    @Transactional
    UserDTO getUserByBadge(Long badgeNumber);
}