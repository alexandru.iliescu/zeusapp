package com.zeus.exceptions;

import static java.util.Collections.singletonList;

import com.zeus.model.util.ResponseMessage;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = CustomApplicationException.class)
    public ResponseMessage handleCustomExceptions(CustomApplicationException e, HttpServletResponse httpResponse) {
        logger.error(e.getMessage(), e);
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setErrors(singletonList(e.getMessage()));
        httpResponse.setStatus(e.getStatusCode());

        return responseMessage;
    }
}