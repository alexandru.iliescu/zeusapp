package com.zeus.controller;

import static com.zeus.model.util.ResponseStatus.FIND_USER_LOG_BY_BADGE_NUMBER_SUCCESS;
import static com.zeus.model.util.ResponseStatus.GET_USER_LOG_BY_ID_SUCCESS;
import static com.zeus.model.util.ResponseStatus.LOG_SAVED_SUCCESSFUL;
import static com.zeus.model.util.ResponseStatus.LOG_UPDATED_SUCCESSFUL;
import static com.zeus.model.util.ResponseStatus.USER_LOG_DELETED_SUCCESS;

import com.zeus.model.dto.UserLogDTO;
import com.zeus.model.util.ResponseMessage;
import com.zeus.service.UserLogService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserLogController {

    private final UserLogService userLogService;

    @Autowired
    public UserLogController(UserLogService userLogService) {
        this.userLogService = userLogService;
    }

    @ApiOperation(value = "Save user log", response = UserLogDTO.class)
    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value = "/api/userLog/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseMessage saveUserLog(@RequestBody UserLogDTO userLogDTO) {
        return new ResponseMessage(userLogService.saveUserLog(userLogDTO), LOG_SAVED_SUCCESSFUL);
    }

    @ApiOperation(value = "Update user log", response = UserLogDTO.class)
    @ResponseStatus(value = HttpStatus.OK)
    @PutMapping("/api/userLog/update")
    public ResponseMessage updateUserLog(@RequestBody UserLogDTO userLogDTO) {
        return new ResponseMessage(userLogService.updateUserLog(userLogDTO), LOG_UPDATED_SUCCESSFUL);
    }

    @ApiOperation(value = "Get user log by id")
    @GetMapping("/api/userLog/id/{id}")
    public ResponseMessage getUserLogById(@PathVariable("id") Long id) {
        return new ResponseMessage(userLogService.getUserLogById(id), GET_USER_LOG_BY_ID_SUCCESS);
    }

    @ApiOperation(value = "Get user log by badge number")
    @GetMapping("/api/userLog/badge/{badgeNumber}")
    public ResponseMessage findByBadge(@PathVariable("badgeNumber") Long badgeNumber) {
        return new ResponseMessage(userLogService.findLogTimeByBadge(badgeNumber),
                FIND_USER_LOG_BY_BADGE_NUMBER_SUCCESS);
    }

    @ApiOperation(value = "Delete user log by id")
    @DeleteMapping("/api/userLog/delete/{id}")
    public ResponseMessage deleteCheckinById(@PathVariable Long id) {
        userLogService.deleteUserLogById(id);
        return new ResponseMessage(null, USER_LOG_DELETED_SUCCESS);
    }
}