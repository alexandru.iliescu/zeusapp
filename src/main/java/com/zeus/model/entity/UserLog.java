package com.zeus.model.entity;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "userLogs")
@Entity
@Data
@NoArgsConstructor
public class UserLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String logType;
    private Date userLogTime;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "userId_fk")
    private User user;

    public UserLog(Long id, String logType, Date userLogTime, User user) {
        this.id = id;
        this.logType = logType;
        this.userLogTime = userLogTime;
        this.user = user;
    }
}