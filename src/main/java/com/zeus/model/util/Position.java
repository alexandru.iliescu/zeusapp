package com.zeus.model.util;

public enum Position {
    ADMINISTRATION,
    DEVELOPER,
    QA,
    UI_UX,
    SCRUM_MASTER,
    PO;

    private String value;

    public static Position getValueOf(String value) {
        switch (value.toLowerCase()) {
            case "administration":
                return ADMINISTRATION;
            case "developer":
                return DEVELOPER;
            case "qa":
                return QA;
            case "ui_ux":
                return UI_UX;
            case "scrum_master":
                return SCRUM_MASTER;
            case "po":
                return PO;
            default:
                return null;
        }
    }

    public String getValue() {
        return value;
    }
}