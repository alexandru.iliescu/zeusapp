package com.zeus.repository;

import com.zeus.model.entity.UserLog;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLogRepository extends JpaRepository<UserLog, Long> {

    @Query(value = "SELECT new UserLog (e.id, e.logType, e.userLogTime, e.user) FROM UserLog e")
    List<UserLog> findLogTimeByBadge(@Param("badge") Long badge);

    @Query(value = "SELECT new UserLog (e.id, e.logType, e.userLogTime, e.user) FROM UserLog e")
    List<UserLog> userReportByBadge(@Param("badge") Long badge);

    @Query(value = "SELECT new UserLog (e.id, e.logType, e.userLogTime, e.user) FROM UserLog e WHERE e.user.badge.badgeNumber = :badgeNumber")
    List<UserLog> findUserDayReportByBadge(@Param("badgeNumber") Long badgeNumber);
}