package com.zeus.exceptions;

public class ReportException extends CustomApplicationException {

    public ReportException(String message, int statusCode) {
        super(message, statusCode);
    }

    public ReportException(String message, Throwable throwable, int statusCode) {
        super(message, throwable, statusCode);
    }

    public ReportException() {
    }
}